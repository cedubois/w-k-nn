from dash import Dash, dcc, html, Input, Output
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_moons
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np
from T2T_cpu import T2T
from sklearn.preprocessing import MinMaxScaler

app = Dash(__name__)

app.layout = html.Div([
    html.H4('Interactive wNN plot'),
    dcc.Graph(id="wnn-classification-x-graph"),
    html.P("Select scale parameter gamma:"),
    dcc.Slider(
        id='wnn-classification-x-slider-gamma',
        min=1, max=100, step=5, value=20,
        marks={i: str(i) for i in range(0,100,10)}),
])


@app.callback(
    Output("wnn-classification-x-graph", "figure"), 
    Input("wnn-classification-x-slider-gamma", "value"))
def train_and_display_model(gamma):
    # To get the same data as in the other figures:
    X, y = make_moons(n_samples=50, noise=0.1, random_state=40)
    X = MinMaxScaler().fit_transform(X)
    X, _, y, __ = train_test_split(X,y.astype(str),test_size=0.2, random_state=42)

    xrange, yrange = build_range(X, mesh_size=.002)
    xx, yy = np.meshgrid(xrange, yrange)
    test_input = np.c_[xx.ravel(), yy.ravel()]
    
    clf = T2T(gamma=gamma, k_nn=None, raise_warning=False, verbose=0).fit(X,y.astype('int8'))
    X_tr = clf.transform_exact(X)
    #
    Z = clf.transform_exact(test_input).argmax(1)
    Z = Z.reshape(xx.shape)
    fig = build_figure(X, X_tr, y, Z, xrange, yrange)

    return fig


def build_range(X, mesh_size=.02, margin=.1):
    """
    Create an x range and a y range for building meshgrid
    """
    x_min = X[:, 0].min() - margin
    x_max = X[:, 0].max() + margin
    y_min = X[:, 1].min() - margin
    y_max = X[:, 1].max() + margin

    xrange = np.arange(x_min, x_max, mesh_size)
    yrange = np.arange(y_min, y_max, mesh_size)
    return xrange, yrange


def build_figure(X, X_tr, y, Z, xrange, yrange):
  
    my_col = ['#45a2ff', '#ff5c44',]

    trace_specs = [
    [X, y, '0', 'Train', 'circle', my_col[0]],
    [X, y, '1', 'Train', 'circle', my_col[1]],
    ]
    
    fig = make_subplots(rows=1, cols=2)
    fig.update_layout(width=2000, height=1000)

    for X, y, label, split, marker, col in trace_specs:
        
        fig.add_trace(
            go.Scatter(
                x=X[y==label, 0], y=X[y==label, 1],
                name=f'{split} Split, Label {label}',
                mode='markers', marker_symbol=marker, marker_color=col,
                marker_size=20, marker_line_width=1.5,
            ),
            row=1, col=1
        )

    fig.add_trace(
        go.Contour(
            x=xrange,
            y=yrange,
            z=Z,
            line_width=4,
            showscale=False,
            colorscale=my_col,
            opacity=0.6,
            name='Score',
            hoverinfo='skip',
            contours=dict(
                    start=0,
                    end=1,
                    size=2,)
        )
    )
            
    trace_specs = [
    [X_tr, y, '0', 'Train', 'circle', my_col[0]],
    [X_tr, y, '1', 'Train', 'circle', my_col[1]],
    ]
            
    for X, y, label, split, marker, col in trace_specs:
        
        fig.add_trace(
            go.Scatter(
                x=X[y==label, 0], y=X[y==label, 1],
                name=f'{split} Split, Label {label}',
                mode='markers', marker_symbol=marker, marker_color=col,
                marker_size=20, marker_line_width=1.5,
            ),
            row=1, col=2
        )
            
        fig.add_trace(
            go.Scatter(x=[0,1], y=[0,1],
                    mode='lines', 
                    line=dict(color='rgb(0, 0, 0)', width=4, dash='dash')),
            row=1, col=2
            )

    return fig

if __name__ == "__main__":
    app.run_server(debug=True)
